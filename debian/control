Source: libgeo-osm-tiles-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libwww-perl,
                     libyaml-perl,
                     rename,
                     perl
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libgeo-osm-tiles-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libgeo-osm-tiles-perl.git
Homepage: https://metacpan.org/release/Geo-OSM-Tiles

Package: libgeo-osm-tiles-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libwww-perl,
         libyaml-perl
Multi-Arch: foreign
Description: module for calculating tile numbers for OpenStreetMap
 Geo::OSM::Tiles provides functions for calculating the path to a map tile at
 OpenStreetMap out of geographic coordinates. The path of a tile at OSM has
 the form $zoom/$tilex/$tiley.png. The numbering scheme is documented in the
 OSM wiki at http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames .
 .
 The package also contains the downloadosmtiles script that allows one to
 conveniently download OSM map tiles from a given OSM permalink URL.
